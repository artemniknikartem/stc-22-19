## 3.2 Spring Configuration

### Цель занятия:

Ознакомиться со способами конфигурации. Научиться конфигурировать Spring двумя способами.
***

### План:
1. Способы конфигурации Spring
2. XML конфигурация
3. Java конфигурация
***

### Способы конфигурации Spring

У Spring есть 4 способа конфигурации:

* Xml конфигурация — ClassPathXmlApplicationContext(”context.xml”);
* Groovy конфигурация — GenericGroovyApplicationContext(”context.groovy”);
* Конфигурация через аннотации с указанием пакета для сканирования — AnnotationConfigApplicationContext(”package.name”);
* JavaConfig — конфигурация через аннотации с указанием класса (или массива классов) помеченного аннотацией @Configuration — AnnotationConfigApplicationContext(JavaConfig.class).

Мы рассмотрим два вида конфигурации Spring - xml и java config
***

### XML конфигурация

В проекте создается специальный файл, в котором описывается создание всех бинов для Spring. В ApplicationContext мы кладем **new ClassPathXmlApplicationContext(ПУТЬ_ДО_ФАЙЛА_ОПИСАНИЯ_БИНОВ)** и после этого, если нам нужен какой либо объект класса компонента, то мы не создаем его, как раньше (через вызов конструктора), а просим его создать Spring.
```XML
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">
    
    <context:property-placeholder location="application.properties"/>
    <!-- Инструкция для Spring как создавать объекты (бины) и что для этого нужно -->
    <bean id="someService" class="services.SomeServiceImpl">
        <constructor-arg name="someArg1" ref="someVal1"/>
        <constructor-arg name="someArg2" ref="someVal2"/>
        <constructor-arg name="someArg3" ref="someVal3"/>
    </bean>

    <bean id="someVal1" class="validators.SomeValidator1"/>

    <bean id="someVal2" class="validators.SomeValidator2">
        <constructor-arg name="someArg1" value="${some_property1}"/>
    </bean>

    <bean id="someVal3" class="validators.SomeValidator3">
        <property name="someArg1" value="${some_property2}"/>
    </bean>
</beans>
```
```JAVA
public class Main {
    public static void main(String[] args) {
        //Настраиваем IoC-контейнер, т.е. передаем Spring`у инструкцию, как создавать бины
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("context.xml");
        //Если нам нужен объект класса, который был описан в context.xml, то мы обращаемся к Spring и просим вернуть нам бин
        //Spring сам его создаст, сконфигурирует и только после этого вернет его нам, уже готовым к использованию
        SomeService someService = applicationContext.getBean(SomeServiceImpl.class);

        someService.someMethod("Test stroke");
    }
}
```
***

### Java конфигурация

В Java конфигурации все настраивается исключительно на Java. Создается специальный класс **ApplicationConfig** (название не принципиально), внутри которого, с помощью аннотаций, прописывается настройка Spring. 
```JAVA


//Аннотация @Configuration - указывает на то, что класс является конфигурационным для Spring
@Configuration
//Аннотация @PropertySource - в ней прописывается путь к файлу, откуда будут подтягиваться properties
@PropertySource("classpath:application.properties") 
// Аннотация @ComponentScan - в данной аннотации прописывается папка/и внутри которой/ых Spring должен искать компоненты, 
// которые попадут в контейнер бинов
@ComponentScan(basePackages = "somePackage") 
public class ApplicationConfig {
    //code
}

// Аннотация @Component - ее маркируются классы, чьи объекты будут создаваться Spring`ом, т.е. чьи объекты будут бинами
@Component 
public class SomeServiceImpl implements SomeService {

    private final SomeValidator1 someValidator1;
    private final SomeValidator2 someValidator2;
    private final SomeValidator3 someValidator3;

    // Аннотация @Autowired - ставится на конструктор или сеттер. Указывает, что Spring должен заинжектить (подставить) 
    // все зависимости сам. 
    // Аннотация @Qualifier - нужна для уточнения. Ведь у интерфейса может быть несколько реализаций, 
    // а Spring сам не умеет определять, какая именно реализация нужна.
    @Autowired 
    public SomeServiceImpl(@Qualifier("someName") SomeValidator1 someValidator1,
    @Qualifier("anotherName") SomeValidator2 someValidator2,
    SomeValidator3 someValidator3) {
        this.someValidator1 = someValidator1;
        this.someValidator2 = someValidator2;
        this.someValidator3 = someValidator3;
    }

    @Override
    public void someMethod(String value) {
        //code
    }
}

public class Main {
    public static void main(String[] args) {
        //Теперь мы Spring передаем не файл "инструкцию", а класс-конфигурацию
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        //Если нам нужен объект класса, у которого стоит аннотация @Component, то мы обращаемся к Spring и просим вернуть нам бин 
        //Spring сам его создаст, сконфигурирует и только после этого вернет его нам, уже готовым к использованию
        SomeService someService = applicationContext.getBean(SomeServiceImpl.class);

        someService.someMethod("Test stroke");
    }
}
```
***

### Заключение

На занятии были разобраны два способа конфигурации Spring.
***