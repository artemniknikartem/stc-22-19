## 1.9 Сложность алгоритмов

O(n), O(log n), O(1), O(n^2)

* **O (Big O Notation)** - относительное представление сложности алгоритма. Показывает, как будет меняться производительность алгоритма при изменении количества входных данных

При увеличении количества входных данных у нас будут изменяться две вещи:
* Время работы алгоритма
* Количество памяти, нужное для выполнения алгоритма

### О(1) - Константное время
```JAVA
//Получаем последний элемент массива
public int getLastElement(int[] array) {
	return array[array.length - 1];
}
```
### О(n) - Линейная сложность
```JAVA
//Складываем все элементы массива и возвращаем сумму
public int sumAllElements(int[] array) {
	int result = 0;

	for (int i = 0; i < array.length; i++) {
		result = result + array[i];
	}
}
```

```JAVA
//Нахождение факториала через рекурсию
public static int factorial(int number) {
        if (number == 1) {
            return number;
        }

        return number * factorial(number - 1);
    }
```

### О(log n) - Логарифмическая сложность
```JAVA
public static int recursiveBinarySearch(int arr[], int elementToSearch, int firstIndex, int lastIndex) {

        // Если последний индекс больше или равен начальному
        if (lastIndex >= firstIndex) {
            //Высчитываем середину между двух индексов
            int mid = firstIndex + (lastIndex - firstIndex) / 2;

            // Если элемент по индексу, который мы высчитали и есть искомый элемент, 
            // то мы его возвращаем
            if (arr[mid] == elementToSearch)
                return mid;

            // Если элемент по индексу, который мы высчитали больше элемента, который мы ищем
            // то вызываем этот же метод, но отбрасываем большую (правую) половину массива
            // т.е. ищем число в левой части массива
            if (arr[mid] > elementToSearch) {
                return recursiveBinarySearch(array, elementToSearch, firstIndex, mid - 1);
            }

            // Если элемент по индексу, который мы высчитали меньше элемента, который мы ищем
            // то вызываем этот же метод, но отбрасываем меньшую (левую) половину массива
            // т.е. ищем число в правой части массива
            return recursiveBinarySearch(array, elementToSearch, mid + 1, lastIndex);
        }
        //Если число, которое мы ищем мы не нашли - то возвращаем -1
        return -1;
    }
```

### О(n^2) - Квадратичная сложность
```JAVA
//Создаем таблицу умножения
public int tableMultiplication(int number) {
	int result = 0;

	for (int i = 0; i <= number; i++) {
		for (int i = 0; j <= number; i++) {
			System.out.println(i * j);
		}
	}
}
```

```JAVA
//Сортировка пузырьком
public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp = array[j];
                    array[j] = array[i];
                    array[i] = temp;
                }
            }
        }
    }
```

> Краткая сноска:
> * Получение элемента коллекции это O(1). Будь то получение по индексу в массиве, или по ключу в словаре в  нотации Big O это будет O(1)
> * Перебор массива/коллекции это O(n)
> * Вложенные циклы по той же структуре данных - это O(n^2)
> * Бинарный поиск (Разделяй и властвуй) - всегда O(log n)
> * Итерации которые используют (Разделяй и властвуй) - это O(n log n)