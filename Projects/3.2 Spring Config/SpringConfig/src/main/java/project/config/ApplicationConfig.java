package project.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import project.validators.PasswordBlackList;
import project.validators.PasswordBlackListImpl;


//Данная аннотация помечает класс, как конфигурационный класс для Spring
@Configuration
//Аннотация @PropertySource позволяет нам подключить файл application.properties
//в котором у нас вынесены настройки и разные значения
@PropertySource("classpath:application.properties")
//Аннотация @ComponentScan настраивает область поиска бинов спринга
@ComponentScan(basePackages = "project")
public class ApplicationConfig {

    //Аннотация @Bean ставится над методами. Используется для создания бинов со сложной логикой
    //инициализации. Если создание бина простое, то ставится аннотация @Component на класс,
    //чей бин нам нужен
//    @Bean
//    public PasswordBlackList passwordBlackList() {
//        PasswordBlackList passwordBlackList = new PasswordBlackListImpl();
//        //настройка1
//        //инициализация поля 2
//        //выполнение каких либо предписаний 3
//        return passwordBlackList;
//    }

}
