package project.validators;

public interface PasswordBlackList {

    boolean contains(String password);
}
