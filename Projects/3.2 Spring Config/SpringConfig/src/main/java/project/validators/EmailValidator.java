package project.validators;

public interface EmailValidator {

    void validate(String email);
}
