package project.validators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//Класс, который проверяет почту на длину
//Аннотация @Component помечает данный класс для спринга
//спринг теперь знает об этом классе, и он будет создавать бины этого класса
@Component("emailValidatorLength")
public class EmailValidatorByLengthImpl implements EmailValidator {

    private final int minLength;

    //@Value - для простановки значений примитивов
    public EmailValidatorByLengthImpl(@Value("${validator.email.minLength}") int minLength) {
        this.minLength = minLength;
    }

    @Override
    public void validate(String email) {
        if (email.length() < minLength) {
            throw new IllegalArgumentException("Длина почты некорректна");
        }
    }
}
