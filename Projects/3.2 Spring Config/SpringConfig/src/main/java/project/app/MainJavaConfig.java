package project.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import project.config.ApplicationConfig;
import project.services.SignUpService;
import project.services.SignUpServiceImpl;

public class MainJavaConfig {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SignUpService signUpService = applicationContext.getBean(SignUpServiceImpl.class);

        signUpService.signUp("abrakadabra@milo.ru", "!23@#&78900");
    }
}
