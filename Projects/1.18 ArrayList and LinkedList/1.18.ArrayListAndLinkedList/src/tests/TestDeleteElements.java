package tests;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestDeleteElements {

    public static void main(String[] args) {
        System.out.println("Удаление по значению");
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();
        addElements(arrayList);
        addElements(linkedList);

        deleteArrayListValue(arrayList, arrayList.size() / 2);
        deleteLinkedListValue(linkedList, linkedList.size() / 2);

        System.out.println("Удаление по индексу");
        deleteArrayListIndex(arrayList, arrayList.size() / 2);
        deleteLinkedListIndex(linkedList, linkedList.size() / 2);
    }

    public static void deleteArrayListValue(ArrayList<Integer> integers, int value) {

        Date start = new Date();
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) == value) {
                integers.remove(i);
                Date end = new Date();
                System.out.println("ArrayList удалил элемент - " + value + " за - " + (end.getTime() - start.getTime()));
                return;
            }
        }
        System.out.println("Элемент не был найден");
    }

    public static void deleteLinkedListValue(LinkedList<Integer> integers, int value) {
        Date start = new Date();
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) == value) {
                integers.remove(i);
                Date end = new Date();
                System.out.println("LinkedList удалил элемент - " + value + " за - " + (end.getTime() - start.getTime()));
                return;
            }
        }
        System.out.println("Элемент не был найден");
    }

    public static void deleteArrayListIndex(ArrayList<Integer> integers, int index) {
        Date start = new Date();
        integers.remove(index);
        Date end = new Date();
        System.out.println("ArrayList удалил элемент по индексу - " + index + " за - " + (end.getTime() - start.getTime()));
    }

    public static void deleteLinkedListIndex(LinkedList<Integer> integers, int index) {
        Date start = new Date();
        integers.remove(index);
        Date end = new Date();
        System.out.println("LinkedList удалил элемент по индексу - " + index + " за - " + (end.getTime() - start.getTime()));
    }

    public static void addElements(List<Integer> integers) {
        for (int i = 0; i < 100_000; i++) {
            integers.add(i);
        }
    }
}
