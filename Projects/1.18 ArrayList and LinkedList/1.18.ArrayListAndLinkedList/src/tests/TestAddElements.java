package tests;

import sun.awt.image.ImageWatched;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

public class TestAddElements {

    public static void main(String[] args) {
        System.out.println("Тестирование на добавление элементов в конец");
        addInArrayListInEnd();
        addInLinkedListEnd();
        System.out.println("******************************");
        System.out.println("Тестирование на добавление элементов в начало");
        //[][][][][][][][][]
        addArrayListInBegin();
        addInLinkedListBegin();
        System.out.println("******************************");
        System.out.println("Тестирование на добавление элементов в середину");
        addInArrayListInMiddle();
        addInLinkedListInMiddle();
    }


    public static void addInArrayListInEnd() {
        ArrayList<Integer> integers = new ArrayList<>();
        Date start = new Date();
        for (int i = 0; i < 100_000; i++) {
            integers.add(i);
        }
        Date end = new Date();

        System.out.println("ArrayList добавил 100_000 элементов в конец за - " + (end.getTime() - start.getTime()));
    }

    public static void addInLinkedListEnd() {
        LinkedList<Integer> integers = new LinkedList<>();

        Date start = new Date();
        for (int i = 0; i < 100_000; i++) {
            integers.add(i);
        }

        Date end = new Date();
        System.out.println("LinkedList добавил 100_000 элементов в конец за - " + (end.getTime() - start.getTime()));
    }


    public static void addArrayListInBegin() {
        ArrayList<Integer> integers = new ArrayList<>();
        Date start = new Date();
        for (int i = 0; i < 100_000; i++) {
            integers.add(0, i);
        }
        Date end = new Date();

        System.out.println("ArrayList добавил 100_000 элементов в начало за - " + (end.getTime() - start.getTime()));
    }

    public static void addInLinkedListBegin() {
        LinkedList<Integer> integers = new LinkedList<>();

        Date start = new Date();
        for (int i = 0; i < 100_000; i++) {
            integers.addFirst(i);
        }

        Date end = new Date();
        System.out.println("LinkedList добавил 100_000 элементов в начало за - " + (end.getTime() - start.getTime()));
    }

    public static void addInArrayListInMiddle() {
        ArrayList<Integer> integers = new ArrayList<>();
        Date start = new Date();
        for (int i = 0; i < 100_000; i++) {
            integers.add(integers.size() / 2, i);
        }
        Date end = new Date();

        System.out.println("ArrayList добавил 100_000 элементов в середину за - " + (end.getTime() - start.getTime()));
    }

    public static void addInLinkedListInMiddle() {
        LinkedList<Integer> integers = new LinkedList<>();

        Date start = new Date();
        //TODO: проверить добавление через итератор
        for (int i = 0; i < 100_000; i++) {
            integers.add(integers.size() / 2, i);
        }

        Date end = new Date();
        System.out.println("LinkedList добавил 100_000 элементов в середину за - " + (end.getTime() - start.getTime()));
    }

}
