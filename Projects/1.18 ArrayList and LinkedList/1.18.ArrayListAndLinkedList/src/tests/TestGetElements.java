package tests;

import sun.awt.image.ImageWatched;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestGetElements {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();
        addElements(arrayList);
        addElements(linkedList);

        getElementInArrayList(arrayList, arrayList.size() / 2);
        getElementInLinkedList(linkedList, linkedList.size()  / 2);
    }


    public static void getElementInArrayList(ArrayList<Integer> integers, int index) {
        Date start = new Date();
        int result = integers.get(index);
        Date end = new Date();
        System.out.println("ArrayList достал элемент - " + result + " по индексу " + index + " за - " + (end.getTime() - start.getTime()));
    }

    public static void getElementInLinkedList(LinkedList<Integer> integers, int index) {
        Date start = new Date();
        int result = integers.get(index);
        Date end = new Date();
        System.out.println("LinkedList достал элемент - " + result + " по индексу " + index + " за - " + (end.getTime() - start.getTime()));
    }
    public static void addElements(List<Integer> integers) {
        for (int i = 0; i < 10_000_000; i++) {
            integers.add(i);
        }
    }
}
