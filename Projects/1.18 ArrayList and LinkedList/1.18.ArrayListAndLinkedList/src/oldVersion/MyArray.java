package oldVersion;

public class MyArray {

    private int[] array;

    private int count;

    private final int DEFAULT_CAPACITY = 10;

    private final int DEFAULT_COUNT = 0;


    public MyArray(int length) {
        this.array = new int[length];
        this.count = DEFAULT_COUNT;
    }

    public MyArray() {
        this.array = new int[DEFAULT_CAPACITY];
        this.count = DEFAULT_COUNT;
    }

    public void addElement(int element) {
        if (count == array.length) {
            int[] tempArray = new int[array.length * 2];
            //TODO: найти метод, которым можно скопировать значения одного массива в другой
            for (int i = 0; i < array.length; i++) {
                tempArray[i] = array[i];
            }

            array = tempArray;
        }
        array[count] = element;
        count++;
    }

    public int[] getArray() {
        return array;
    }

    public int getCount() {
        return count;
    }
}
