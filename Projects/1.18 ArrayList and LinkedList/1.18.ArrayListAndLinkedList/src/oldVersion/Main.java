package oldVersion;

import java.util.*;

public class Main {
    public static void main(String[] args) {
//        int[] array = new int[10];
//        Random random = new Random();
//        for (int i = 0; i < 15; i++) {
//            array[i] = random.nextInt(100);
//        }
//
//        System.out.println(Arrays.toString(array));

//        MyArray myArray = new MyArray();
//        Random random = new Random();
//        int counts = random.nextInt(100);
//        for (int i = 0; i < counts; i++) {
//            myArray.addElement(i);
//        }
//
//        System.out.println(Arrays.toString(myArray.getArray()));
//
//        List<Integer> arrayList = new ArrayList<>();
        Random random = new Random();
        int counts = random.nextInt(100);
//        for (int i = 0; i < counts; i++) {
//            arrayList.add(i);
//        }
//
//        System.out.println(Arrays.asList(arrayList));

        //Балуемся со связанным списком
        LinkedList<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < counts; i++) {
            linkedList.add(i);
        }

        System.out.println(Arrays.asList(linkedList));

        System.out.println(linkedList.getFirst());
        System.out.println(linkedList.getLast());
        System.out.println(linkedList.removeFirst());
        System.out.println(linkedList.removeLast());
        System.out.println(Arrays.asList(linkedList));
    }
}
