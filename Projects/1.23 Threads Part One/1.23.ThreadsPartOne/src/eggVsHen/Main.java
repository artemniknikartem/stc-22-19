package eggVsHen;

public class Main {
    public static void main(String[] args) {
        EggThread eggThread = new EggThread();
        HenThread henThread = new HenThread();

        eggThread.start();
        henThread.start();

        System.out.println("Конец метода main");
    }
}
