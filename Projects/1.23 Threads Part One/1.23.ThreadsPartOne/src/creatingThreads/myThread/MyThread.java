package creatingThreads.myThread;

//1. способ - наследуемся от класса Thread, и переопределяем метод run().
public class MyThread extends Thread {
    //TODO: скачать плагин для многопоточности, что бы посмотреть потоки
    //И метод .start() запускает поток, и метод .run() запускает поток
    //всегда переопределяем метод .run()
    @Override
    public void run() {
        System.out.println("Плагин нужно скачать");
        helpMethod();
    }

    private void helpMethod() {
        System.out.println("Мы в вспомогательном методе");
    }
}
