package workers;

public class Worker extends Thread {

    public Worker(String name) {
        super(name);
        //start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Я - " + Thread.currentThread().getName() + ", выполняю свою часть работы");
            //Заставляет текущий поток уступить место другому
            try {
                sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            yield();
        }
    }
}
