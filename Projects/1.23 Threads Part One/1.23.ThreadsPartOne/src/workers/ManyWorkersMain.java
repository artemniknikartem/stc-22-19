package workers;

public class ManyWorkersMain {
    public static void main(String[] args) throws InterruptedException {
        //TODO: прочитать про различие параллелизма и асинхронного программирования.
        Worker worker1 = new Worker("Копатель");
        Worker worker2 = new Worker("Таскатель");
        Worker worker3 = new Worker("Закапыватель");

        worker1.start();
        //Метод .join() заставляет текущий поток (в котором вызван метод)
        //ждать завершения работы потока, у чьего объекта вызван метод .join()
        worker1.join();
        worker2.start();
        worker2.join();
        //Поток может быть - демоном. Это означает, что поток вспомогательный
        //Т.е. не нужно ждать его завершения
        //Т.е. когда закончат свою работу все основные потоки, приложение завершится
        //и не важно, выполнил до конца свою работу вспомогательный поток или нет


        worker3.setDaemon(true);
        worker3.start();
        System.out.println("Прораб: Ну все, работа есть, я поехал");
    }
}
