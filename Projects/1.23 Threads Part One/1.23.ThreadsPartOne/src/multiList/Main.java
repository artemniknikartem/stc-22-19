package multiList;

import java.util.List;
import java.util.Vector;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        //Vector - потокозащищенная структура данных
        //TODO: concurrences collection
        List<Integer> integerList = new Vector<>();

        Thread positiveThread = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                integerList.add(i);
            }
        });


        Thread negativeThread = new Thread(() -> {
           for (int i = 0; i < 100; i++) {
               integerList.add(i - 100);
           }
        });

        positiveThread.start();
        negativeThread.start();
        positiveThread.join();
        negativeThread.join();

        int i = 0;
    }
}
