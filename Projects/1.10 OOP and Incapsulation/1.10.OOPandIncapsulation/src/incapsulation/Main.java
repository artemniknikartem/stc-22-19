package incapsulation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        String lastName = scanner.next();
        int age = scanner.nextInt();

        Human human; //объявление переменной
        human = new Human(name, lastName, age); //инициализация переменной - это когда
        //объявили переменную и положили в нее значение


        System.out.println(human.getName());
        System.out.println(human.getLastName());
        System.out.println(human.getAge());
        System.out.println("*************");
        human.setAge(-100);
        System.out.println(human.getAge());


    }
}
