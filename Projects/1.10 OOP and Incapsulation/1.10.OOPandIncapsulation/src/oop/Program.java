package oop;

public class Program {
    public static void main(String[] args) {
        //Ctrl + p
        TestClass testClass = new TestClass("Text", 10, true);

        System.out.println(testClass.text);
        System.out.println(testClass.number);
        System.out.println(testClass.isTrue);

        //ТИП_ПЕРЕМЕННОЙ (КЛАСС) НАЗВАНИЕ_ОБЪЕКТНОЙ_ПЕРЕМЕННОЙ
        //= - оператор присвоения
        //new - оператор выделения памяти под объект
        //Main() - пустой конструктор класса Main()
        TestClass testClass1 = new TestClass("Another Text", 100, false);


//        main1.text = "Hello all";
//        main1.isTrue = true;
//        main1.number = 100;

        System.out.println("***************************");
        System.out.println(testClass1.text);
        System.out.println(testClass1.number);
        System.out.println(testClass1.isTrue);


        testClass.sayHello();
    }
}
