package oldVersion;

//Имплементация интерфейса UtilNumbers
//Метод checkNumbers проверяет на четность (в консоль будут выводиться четные числа)
public class EvenNumbers implements UtilNumbers {

    @Override
    public void checkNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.println(numbers[i] + " - четное число");
            }
        }
    }
}
