package newVersion.lyambda;

//Интерфейс для работы с числами
public interface NumberUtils {

    void checkNumbers(int[] array);

    //Метод с реализацией по умолчанию - его не обязательно реализовывать в классе
    default void testMethod() {
        System.out.println("Test");
    }
}
