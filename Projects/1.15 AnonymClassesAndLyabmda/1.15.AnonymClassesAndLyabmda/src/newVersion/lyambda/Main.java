package newVersion.lyambda;

import newVersion.lyambda.NumberUtils;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10) + 1;
        }

        /*
         * Лямбда выражение:
         * (ПАРАМЕТРЫ_МЕТОДА_ФУНКЦИОНАЛЬНОГО_ИНТЕРФЕЙСА) -> ВЫЧИСЛЕНИЯ ИЛИ ЖЕ РЕАЛИЗАЦИЯ_МЕТОДА_ФУНКЦИОНАЛЬНОГО_ИНТЕРФЕЙСА
         */
        printNumbers(array, (numbers -> {
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] % 2 == 0) {
                    System.out.println(numbers[i] + " - четное число");
                }
            }
        }));

        printNumbers(array, (numbers -> {
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] % 2 != 0) {
                    System.out.println(numbers[i] + " - нечетное число");
                }
            }
        }));
    }

    public static void printNumbers(int[] array, NumberUtils numberUtils) {
        numberUtils.checkNumbers(array);
    }
}
