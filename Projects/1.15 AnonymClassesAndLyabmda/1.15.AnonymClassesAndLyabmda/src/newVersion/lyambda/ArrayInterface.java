package newVersion.lyambda;
//Тестовый интерфейс, метод которого принимает на вход неопределенное количество чисел, но ничего не возвращает
public interface ArrayInterface {

    void sumNumbers(int ... numbers);
}
