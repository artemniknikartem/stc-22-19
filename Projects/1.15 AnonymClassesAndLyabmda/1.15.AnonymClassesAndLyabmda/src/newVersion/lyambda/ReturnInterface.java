package newVersion.lyambda;

//Тестовый интерфейс, метод которого принимает на вход два аргумента и возвращает целое число
public interface ReturnInterface {

    int multiplication(int numberOne, int numberTwo);
}
