package newVersion.lyambda;

public class ForLyambds {

//    public static void main(String[] args) {
//        testLymbda(numbers -> {
//            int result = 0;
//            for (int i = 0; i < numbers.length; i++) {
//                System.out.print("[" + numbers[i] + "]");
//                result = result + numbers[i];
//                if (i == numbers.length - 1) {
//                    System.out.println();
//                }
//            }
//            System.out.println(result);
//        }, 10, 21, 54, 67, 46, 33, 79, 90);
//    }
//
//    public static void testLyambda(ArrayInterface arrayInterface, int ... numbers) {
//        arrayInterface.sumNumbers(numbers);
//    }
// ***************************************************
//    public static void main(String[] args) {
//        testLyambda(10, 145, ((numberOne, numberTwo) -> System.out.println(numberOne * numberTwo)));
//    }
//
//    public static void testLyambda(int number1, int number2, TwoArgumentsInterface twoArgumentsInterface) {
//        twoArgumentsInterface.multiplication(number1, number2);
//    }
// ***************************************************
//    public static void main(String[] args) {
//        testLyambda(() -> System.out.println("Привет всем, мы на уроке по программированию"));
//    }
//
//    public static void testLyambda(VoidInterface voidInterface) {
//        voidInterface.printSomething();
//    }
//***************************************************

    public static void main(String[] args) {
        System.out.println(testLyambda(10, 145, ((numberOne, numberTwo) -> {
            int result = numberOne * numberTwo;
            return result;
        })));
    }

    public static int testLyambda(int numberOne, int numberTwo, ReturnInterface returnInterface) {
        int result = returnInterface.multiplication(numberOne, numberTwo);
        return result;
    }

}
