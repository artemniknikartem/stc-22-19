package oneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OneToOneMain {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {

            Passport passport = new Passport("0987 654321");
            Person person = Person.builder()
                    .name("Ivan")
                    .lastName("Ivanov")
                    .passport(passport)
                    .build();

            session.save(passport);
            session.save(person);
        }
    }
}
