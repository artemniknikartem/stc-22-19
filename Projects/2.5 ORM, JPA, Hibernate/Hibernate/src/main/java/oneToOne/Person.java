package oneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String lastName;

    //ОДИН - К - ОДНОМУ
    //Один объект данного класса (Person) может иметь один объект класса Passport
    @OneToOne
    private Passport passport;
}
