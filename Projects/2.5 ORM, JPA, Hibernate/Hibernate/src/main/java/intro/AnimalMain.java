package intro;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AnimalMain {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        //Скармливаем Hibernate наши настройки
        configuration.configure();
        //Создаем "фабрику", которая будет нам отдавать сессии. Сессия - физическое соединение нашего
        //приложения с бд
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
        ) {
            //Открываем сессию (открываем физическое соединение для работы с бд)
            Session session = sessionFactory.openSession();
            Animal animal = Animal.builder()
                    .name("Markiza")
                    .color("White")
                    .type("Cat")
                    .build();

            session.save(animal);
            session.close();
            System.out.println("Животное добавлено");
        }
    }
}
