package intro;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//У JPA есть требования к классам, которые мы хотим сохранять в бд
//1. Класс обязательно должен обладать аннотацией @Entity
//2. У класса обязательно должно быть как минимум одно поле (их может быть несколько), которое будет
// отвечать за уникальность (которое будет идентификатором/ключом). В классе должна присутствовать аннотация
// @Id у как минимум одного поля
//3. У класса должен быть пустой конструктор.
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String color;
    private String type;
}
