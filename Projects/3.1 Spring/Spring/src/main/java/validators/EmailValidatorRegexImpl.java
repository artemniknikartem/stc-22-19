package validators;

import java.util.regex.Pattern;

public class EmailValidatorRegexImpl implements EmailValidator {

    //Паттерн нужен для регулярных выражений
    //Мы закидываем шаблон строки, как она должна выглядеть
    private Pattern pattern;

    public void setPattern(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    //Проверяем почту на наличие спецсимвола '@'
    @Override
    public void validate(String email) {
        if (!pattern.matcher(email).find()) {
            throw new IllegalArgumentException("Некорректная почта. Нет '@'");
        }
    }
}
