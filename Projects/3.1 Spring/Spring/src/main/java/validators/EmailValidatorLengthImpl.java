package validators;

public class EmailValidatorLengthImpl implements EmailValidator {

    //Минимальная длина почты
    private int minLength;

    public EmailValidatorLengthImpl(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public void validate(String email) {
        if (email.length() < minLength) {
            throw new IllegalArgumentException("Длина почты некорректна");
        }
    }
}
