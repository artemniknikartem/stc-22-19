package app;

import services.SignUpService;
import services.SignUpServiceImpl;
import validators.*;

public class MainNotSpring {
    public static void main(String[] args) {
        EmailValidatorRegexImpl emailValidator = new EmailValidatorRegexImpl();
        emailValidator.setPattern(".+@.+");
        PasswordValidator passwordValidator = new PasswordValidatorCharacterImpl();
        PasswordBlackList passwordBlackList = new PasswordBlackListImpl();
        SignUpService signUpService = new SignUpServiceImpl(emailValidator, passwordValidator, passwordBlackList);

        signUpService.signUp("ivan@mail.ru", "1!345&789@1");
    }
}
