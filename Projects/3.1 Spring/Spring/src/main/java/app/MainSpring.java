package app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import services.SignUpService;
import services.SignUpServiceImpl;

public class MainSpring {
    public static void main(String[] args) {
        //IoC-контейнер
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("context.xml");
        SignUpService signUpService = applicationContext.getBean(SignUpServiceImpl.class);
        //SignUpService signUpService = (SignUpService) applicationContext.getBean("signUpService");

        signUpService.signUp("abrakadabramail.ru", "123456789012");
    }
}
