## 3.1 Spring

* **Spring Framework** - фреймворк (набор библиотек), облегчающий работу прогрммиста.

Spring обеспечивает гибкость приложения, за счет того, что разделяет момент связывания компонентов и момент использования данных компонентов.

Создание и связывание компонентов происходит в специальном модуле - контейнере бинов.

Контейнер бинов (IoC-контейнер) - объект Spring, который содержит в себе информацию обо всех компонентах системы.

Бин - используется для ссылки на любой компонент. Создается контейнером.

IoC/DI

Inversion of Control - подход, который позволяет конфигурировать и управлять объектами с помощью рефлексии. 
Вместо ручного внедрения зависимостей фреймворк забирает это дело на себя, через IoC-контейнер.

Dependency Injection - один из способов реализации IoC в Spring. Это шаблон проектирования, в котором контейнер передает экземпляры объектов
по их типу другим объектам с помощью конструктора или сеттера, что позволяет слабо связанный код