package poker;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        map.put("Oleg", 26);
        map.put("Igor", 20);
        map.put("Vladimir", 1);
        map.put("Varvara", 2);

        System.out.println(map.get("Oleg"));
        System.out.println(map.get("Igor"));
        System.out.println(map.get("Vladimir"));
        System.out.println(map.get("Varvara"));
    }
}
