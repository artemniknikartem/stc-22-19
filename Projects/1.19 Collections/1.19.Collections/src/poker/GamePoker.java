package poker;

import java.util.*;

public class GamePoker {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Card[] cards = new Card[5];

        //K, K, 10, 9 ,8 - пара
        //K, K, 10, 10 ,8 - две пары
        //K, K, К, 9 ,8 - сет
        //К, K, K, 5, 5 - full house
        //К, K, K, K, 5 - каре

        for (int i = 0; i < cards.length; i++) {
            cards[i] = new Card(scanner.next());
        }

        Map<Card, Integer> countCards = new HashMap<>();

        for (int i = 0; i < cards.length; i++) {
            //у map есть метод .containsKey(КЛЮЧ)
            //проверяем, есть ли в нашей структуре данных данный ключ (т.е. клали ли мы уже, что то, с таким ключом)
            if (countCards.containsKey(cards[i])) {
                //Если, мы с таким ключом уже что то клали, значит, данный ключ уже хранит какое то значение
                //в данной задаче он хранит количество данной карты на столе
                //Значит мы берем, и просим вернуть значение. которое хранит данный ключ
                int temp = countCards.get(cards[i]);
                temp = temp + 1;
                countCards.put(cards[i], temp);
            } else {
                countCards.put(cards[i], 1);
            }

        }

        switch (countCards.size()) {
            case 1:
                System.out.println("Жулик! Все пять карт - одинаковы!");
                break;
            case 2:
                if (checkValues(countCards, 4)) {
                    System.out.println("На столе комбинация КАРЕ");
                } else {
                    System.out.println("На столе комбинация ФУЛЛ ХАУЗ");
                }
                break;
            case 3:
                if (checkValues(countCards, 3)) {
                    System.out.println("На столе комбинация СЕТ");
                } else {
                    System.out.println("На столе комбинация ДВЕ ПАРЫ");
                }
                break;
            case 4:
                System.out.println("На столе комбинация ПАРА");
                break;
            case 5:
                System.out.println("Лошара");
                break;
        }
    }

    public static boolean checkValues(Map<Card, Integer> map, int number) {
        List<Integer> list = new ArrayList<>(map.values());
        //for-each
        //for - это синтаксический сахар над циклом while
        //for each - это синтаксический сахар над циклом for
        //for (ТИП ПЕРЕМЕННОЙ НАЗВАНИЕ ПЕРЕМЕННОЙ : НАИМЕНОВАНИЕ КОЛЛЕКЦИИ)
//        for (int i = 0; i < list.size(); i++) {
//            Integer integer = list.get(i);
//        }
        for (Integer integer : list) {
            if (integer == number) {
                return true;
            }
        }
        return false;
    }
}
