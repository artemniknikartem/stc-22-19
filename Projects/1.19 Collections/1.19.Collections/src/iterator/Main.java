package iterator;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> integerSet = new HashSet<>();
        Random random = new Random();

        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));
        integerSet.add(random.nextInt(100));

        Iterator<Integer> integerIterator = integerSet.iterator();

        // [],[],[],[],[],[]
        //                  ^
        while (integerIterator.hasNext()) {
            Integer integer = integerIterator.next();
            System.out.println(integer);
        }
    }
}
