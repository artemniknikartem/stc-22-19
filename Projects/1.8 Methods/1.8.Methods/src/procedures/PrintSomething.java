package procedures;

public class PrintSomething {
    public static void main(String[] args) {
        printSomething("");
    }
    //Void - пустота. Если мы в контракте видим слово "void", то данный метод (данная подпрограмма)
    //является процедурой
    public static void printSomething(String text) {
        if (text.equals("")) {
            System.out.println("Вы ничего не передали на печать");
            return;
        }
        //System.out.println(TEXT) - выводит в консоль какой либо TEXT
        System.out.println(" Мне сказали напечатать вот это - " + text);
    }
}
