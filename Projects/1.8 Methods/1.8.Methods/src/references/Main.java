package references;
//В методы аргументы всегда передаются по значению
public class Main {

//    public static void main(String[] args) {
//        int number = 10;
//        System.out.println("До вызова метода sum, число number = " + number);
//        sum(number);
//        System.out.println("После вызова метода sum, число number = " + number);
//    }
//
//    public static void sum(int number) {
//        number = number + number;
//    }

    public static void main(String[] args) {
        int[] array = new int[10];

        System.out.println("Нулевой элемент массива до вызова функции = " + array[0]);
        sum(array);
        System.out.println("Нулевой элемент массива после вызова функции = " + array[0]);
    }

    public static void sum(int[] array) {
        array[0] = 109;
    }
}
