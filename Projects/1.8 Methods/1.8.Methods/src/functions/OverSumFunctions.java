package functions;

public class OverSumFunctions {

//    public static void main(String[] args) {
//        int result = sumOneNumbers(120);
//        System.out.println(result);
//    }
//
//    public static int sumTwoNumbers(int numberOne, int numberTwo) {
//        int resultSum = numberOne + numberTwo;
//        return resultSum;
//    }
//
//    public static int sumThreeNumbers(int numberOne, int numberTwo, int numberThree) {
//        int resultSum = numberOne + numberTwo + numberThree;
//        return resultSum;
//    }
//
//    public static int sumOneNumbers(int numberOne) {
//        int resultSum = numberOne + numberOne;
//        return resultSum;
//    }

    public static void main(String[] args) {
        int resultSum = sum(10);
        System.out.println(resultSum);
    }

    public static int sum(int numberOne) {
        int resultSum = numberOne + numberOne;
        return resultSum;
    }

    public static int sum(int numberOne, int numberTwo) {
        int resultSum = numberOne + numberTwo;
        return resultSum;
    }

    public static int sum(int numberOne, int numberTwo, int numberThree) {
        int resultSum = numberOne + numberTwo + numberThree;
        return resultSum;
    }

    public static int sum(double numberOne, int numberTwo) {
        int resultSum = (int) numberOne + numberTwo;
        return resultSum;
    }

}
