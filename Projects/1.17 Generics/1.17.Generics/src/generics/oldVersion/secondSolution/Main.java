package generics.oldVersion.secondSolution;


public class Main {
    public static void main(String[] args) {
        MailBox mailBox = new MailBox();

        Box box = new Box("Для Олега");

        mailBox.setBox(box);

        System.out.println(mailBox.getBox().getName());

    }
}
