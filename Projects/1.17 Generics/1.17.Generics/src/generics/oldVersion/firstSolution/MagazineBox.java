package generics.oldVersion.firstSolution;

public class MagazineBox {

    private Magazine magazine;

    public MagazineBox(Magazine magazine) {
        this.magazine = magazine;
    }

    public Magazine getMagazine() {
        return magazine;
    }

    public void setMagazine(Magazine magazine) {
        this.magazine = magazine;
    }
}
