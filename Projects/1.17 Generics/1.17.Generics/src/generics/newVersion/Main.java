package generics.newVersion;

public class Main {

    public static void main(String[] args) {
//        MailBox<Distribution> mailBox = new MailBox<>(new Mail("Hello all"));
//
//        mailBox.setDistribution(new Magazine("Мурзилка"));
//
//        //В mailBox мы указали, что мы работаем с Distribution и с его потомками
//        //Мы можем в ящик положить и письмо, и журнал, и бандероль
//        //Мы никак не можем с этим взаимодействовать, т.к. класс Distribution - пуст
//        //В нем нет никаких методов
//        //TODO: исправить эту ситуацию
//        System.out.println(mailBox.getDistribution());

        MailBox mailBox = new MailBox(new Car("Audi", "A6"));

        System.out.println(mailBox.getDistribution());

    }
}
