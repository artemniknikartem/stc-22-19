package wrappers;

public class Main {
    public static void main(String[] args) {

        //Wrappers (Классы - обертки)
        //У каждого примитива есть свой класс обертка:
        //Для int - Integer
        //double - Double
//        StorageDigits<Integer> carStorageDigits = new StorageDigits<>();
//
//        int count = 1;
//
//        for (int i = 0; i < 15; i++) {
//            System.out.println(count
//                    + " итерация: добавили машину. Результат - "
//                    + carStorageDigits.addElement(i));
//            count++;
//        }

        Integer number = 15; //autoboxing
        Integer integer = new Integer("10");
        System.out.println(integer);

        int anotherNumber = number; //autounboxing

        System.out.println(anotherNumber);

    }
}
