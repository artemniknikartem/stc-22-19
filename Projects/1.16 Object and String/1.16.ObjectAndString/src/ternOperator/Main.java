package ternOperator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int result = scanner.nextInt();

//        if (result >= 0) {
//            System.out.println("Вы ввели положительное число");
//        } else {
//            System.out.println("Вы ввели отрицательное число");
//        }

        //УСЛОВИЕ ? ЧТО ВЕРНУТЬ, ЕСЛИ УСЛОВИЕ ВЕРНО : ЧТО ВЕРНУТЬ, ЕСЛИ УСЛОВИЕ НЕВЕРНО
//        String text = result >= 0 ? "Вы ввели положительное число" : "Вы ввели отрицательное число";
//        System.out.println(text);
        System.out.println(result >= 0 ? "Вы ввели положительное число" : "Вы ввели отрицательное число");
    }
}
