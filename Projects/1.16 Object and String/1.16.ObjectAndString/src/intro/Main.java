package intro;

public class Main {

    public static void main(String[] args) {
        Car car = new Car();
        Dog dog = new Dog();
        Human human = new Human();
        Tank tank = new Tank();

        Object[] objects = new Object[4];

        objects[0] = car;
        objects[1] = dog;
        objects[2] = human;
        objects[3] = tank;

    }
}
