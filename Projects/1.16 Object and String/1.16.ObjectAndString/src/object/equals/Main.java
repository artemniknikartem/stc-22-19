package object.equals;

public class Main {

    public static void main(String[] args) {
        Car bmw = new Car("BMW", "X5");
        Car audi = new Car("Audi", "A6");
        Car anotherBmw = new Car("BMW", "X5");

        System.out.println(bmw.equals(audi));
    }
}
