package object.hashCode;

public class Main {

    public static void main(String[] args) {
        Home home = new Home("Земля", 10);
        Home anotherHome = new Home("Бетон", 2);

        System.out.println(home.hashCode());
        System.out.println(anotherHome.hashCode());
    }
}
