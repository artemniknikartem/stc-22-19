package object.hashCode;

import java.util.Objects;

public class Home {

    private String fundament;
    private int countLevels;

    public Home(String fundament, int countLevels) {
        this.fundament = fundament;
        this.countLevels = countLevels;
    }

    public String getFundament() {
        return fundament;
    }

    public void setFundament(String fundament) {
        this.fundament = fundament;
    }

    public int getCountLevels() {
        return countLevels;
    }

    public void setCountLevels(int countLevels) {
        this.countLevels = countLevels;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fundament, countLevels);
    }
}
