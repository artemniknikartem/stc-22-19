package credit_card;


public class Person extends Thread {

    private final String namePerson;
    private final CreditCard card;

    public Person(String namePerson, CreditCard card) {
        this.namePerson = namePerson;
        this.card = card;
        start();
    }

    //TODO: best practice данной проблемы
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException();
            }
            synchronized (card) {
                System.out.println(namePerson + " идет совершать покупку");
                if (card.buy(10)) {
                    System.out.println(namePerson + " совершил покупку");
                } else {
                    System.out.println(namePerson + ": Лебовски, где деньги?????");
                }
            }
        }
    }
}
