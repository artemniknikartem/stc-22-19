package semaphore;


import java.util.concurrent.Semaphore;

public class Car extends Thread {

    private Semaphore semaphore;
    private String number;

    public Car(Semaphore semaphore, String number) {
        this.semaphore = semaphore;
        this.number = number;
        start();
    }

    @Override
    public void run() {
        System.out.println(this.getNumber() + " стоит в очереди на автомойку");

        try {
            //Забираем пропуск
            semaphore.acquire();
            System.out.println(this.getNumber() + " заехал в бокс");
            sleep(1000);
            System.out.println(this.getNumber() + " моется");
            sleep(1000);
            System.out.println(this.getNumber() + " помылся");
            sleep(1000);
            System.out.println(this.getNumber() + " выехал из бокса");
            //Отдаем пропуск
            semaphore.release();
        } catch (InterruptedException e) {
            throw new RuntimeException();
        }
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public void setSemaphore(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
