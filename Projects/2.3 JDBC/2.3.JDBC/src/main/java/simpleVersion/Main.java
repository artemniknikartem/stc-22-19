package simpleVersion;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class Main {
//    public static void main(String[] args) throws SQLException {
//        //Получаем соединение с базой данных
//        Connection connection = DriverManager.getConnection(
//                "jdbc:postgresql://localhost:5432/for_lesson",
//                "postgres",
//                "password"
//        );
//
//        //Интерфейс, через который мы будем отправлять запросы
//        Statement statement = connection.createStatement();
//        //ResultSet хранит в себе результат запроса в бд
//        ResultSet resultSet = statement.executeQuery("SELECT * FROM car");
//
//        List<Car> cars = new ArrayList<>();
//        int id;
//        String brand;
//        String model;
//        String color;
//        int driverId;
//        while(resultSet.next()) {
//            //id = Integer.parseInt(resultSet.getString("id));
//            id = resultSet.getInt("id");
//            brand = resultSet.getString("brand");
//            model = resultSet.getString("model");
//            color = resultSet.getString("color");
//            driverId = resultSet.getInt("driver_id");
//
//            Car car = new Car(id, brand, model, color, driverId);
//            cars.add(car);
//        }
//
//        System.out.println(cars.size());
//    }
    //TODO: убрать проброс вверх (заменить try/catch) в DBConnection
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        DBConnection dbConnection = new DBConnection();
        Statement statement = dbConnection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM car");

        List<Car> cars = new ArrayList<>();
        int id;
        String brand;
        String model;
        String color;
        int driverId;
        while (resultSet.next()) {
            //id = Integer.parseInt(resultSet.getString("id));
            id = resultSet.getInt("id");
            brand = resultSet.getString("brand");
            model = resultSet.getString("model");
            color = resultSet.getString("color");
            driverId = resultSet.getInt("driver_id");

            Car car = new Car(id, brand, model, color, driverId);
            cars.add(car);
            System.out.println(car);
        }
    }
}
