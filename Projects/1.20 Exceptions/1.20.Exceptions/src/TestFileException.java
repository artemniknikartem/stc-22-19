import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestFileException {
    public static void main(String[] args) throws FileNotFoundException {
        readFile();
    }

    public static void readFile() throws FileNotFoundException {
//        File file = new File("fileForTest.txt");
//        try {
//            Scanner scanner = new Scanner(file);
//            System.out.println("File found");
//        } catch (FileNotFoundException e) {
//            System.out.println("File not found");
//            throw new RuntimeException();
//        }
//
//        System.out.println("Мы после блока try-catch");

        File file = new File("fileForTest.txt");
        Scanner scanner = new Scanner(file);
        System.out.println("File found");
    }
}
