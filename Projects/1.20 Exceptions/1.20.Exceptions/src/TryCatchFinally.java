import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatchFinally {
    public static void main(String[] args) {
        int a = 10;
        Scanner scanner = new Scanner(System.in);
        try {

        } catch (InputMismatchException e) {

        } finally {
            a = 256;
        }
        System.out.println(a);
    }
}
