import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class HierarchyException {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a;

        try {
            a = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Мы в InputMismatchException");
        } catch (RuntimeException e) {
            System.out.println("Мы в RuntimeException");
        } catch (Exception e) {
            System.out.println("Мы в Exception");
        }
    }
}
