package lection;

public class NumberUtil implements NumberToBooleanVector{

    public boolean isSimple(int number) {
        if (number == 2 || number == 3) {
            return true;
        }
        //11 -> 2, 3, 4, 5
//        for (int i = 2; i < number; i++) {
//            if (number % i == 0) {
//                return false;
//            }
//        }
        //TODO: почему здесь квадраты
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    public int gcd(int a, int b) {
        //TODO: разобрать, как это чудо работает
        while (b != 0) {
            int temp = a % b;
            a = b;
            b = temp;
        }

        return a;
    }

    @Override
    public boolean map(int number) {
        return isSimple(number);
    }
}
