package lection;

import java.util.ArrayList;
import java.util.List;

public class NumbersProcessor {

    private NumberToBooleanVector numberToBooleanVector;

    public NumbersProcessor(NumberToBooleanVector numberToBooleanVector) {
        this.numberToBooleanVector = numberToBooleanVector;
    }

    public List<Boolean> convertToBooleanVector(List<Integer> numbers) {
        List<Boolean> result = new ArrayList<>();
        //Пробегаемся по всему списку чисел
        //И добавляем результат работы метода map(внутри которого isSimple) в список result
        for (Integer integer : numbers) {
            result.add(numberToBooleanVector.map(integer));
        }

        return result;
    }
}
