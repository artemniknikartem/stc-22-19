package lection;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
//@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumberUtilTest {
    //TODO: Что делать с процедурами?
    private final NumberUtil numberUtil = new NumberUtil();

    @Nested
    @DisplayName("Тестирование метода .isSimple()")
    class IsSimple {
        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 13, 17, 19, 31})
        @DisplayName("Тест простых чисел:")
        public void test_is_simple(int number) {
            assertTrue(numberUtil.isSimple(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {9, 15, 18, 32})
        @DisplayName("Тест не простых чисел:")
        public void testNotSimple(int number) {
            assertFalse(numberUtil.isSimple(number));
        }
    }

    @Nested
    @DisplayName("Тестирование метода .gcd()")
    class TestGcd {

        @ParameterizedTest(name = "for numbers {0} and {1} result - {2}")
        @CsvSource(value = {"9, 12, 3", "18, 12, 6", "16, 48, 16"})
        @DisplayName("Тест наибольшего делителя:")
        public void testGcd(int number1, int number2, int expected) {
            assertEquals(expected, numberUtil.gcd(number1, number2));
        }
    }
}