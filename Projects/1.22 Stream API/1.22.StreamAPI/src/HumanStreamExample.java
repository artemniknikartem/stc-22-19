import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HumanStreamExample {
    public static void main(String[] args) {
        File file = new File("DataBaseHuman.txt");
        List<Human> humans = new ArrayList<>();

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            if (file.createNewFile()) {
                System.out.println("Файл создан");
            } else {
                System.out.println("Файл уже существует");
            }

            writer.write("Oleg|Igonin|male|26\n");
            writer.write("Vasya|Pupkin|male|65\n");
            writer.write("Iezekel|Dzhabatobich|male|12\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }

//        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
//            String human;
//            while ((human = reader.readLine()) != null) {
//                String[] infoAboutHuman = human.split("\\|");
////                String name = infoAboutHuman[0];
////                String lastName = infoAboutHuman[1];
////                String sex = infoAboutHuman[2];
////                int age = Integer.parseInt(infoAboutHuman[3]);
////                Human newHuman = new Human(name, lastName, sex, age);
//                humans.add(new Human(infoAboutHuman[0],
//                        infoAboutHuman[1],
//                        infoAboutHuman[2],
//                        Integer.parseInt(infoAboutHuman[3])));
//            }
//        } catch (IOException e) {
//            throw new RuntimeException();
//        }

        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            humans = reader.lines()
                    .map(human -> {
                        String[] infoAboutHuman = human.split("\\|");
                        String name = infoAboutHuman[0];
                        String lastName = infoAboutHuman[1];
                        Human.Sex sex = Enum.valueOf(Human.Sex.class, infoAboutHuman[2].toUpperCase());
                        int age = Integer.parseInt(infoAboutHuman[3]);

                        return new Human(name, lastName, sex, age);
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException();
        }

        humans.stream()
                .filter(human -> human.getAge() < 30)
                .map(human -> human.toString())
                .forEach(System.out::println);
    }
}
