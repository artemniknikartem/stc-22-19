package autofactory.notInheritance;

public final class Human extends Monkey {

    private String lastName;

    public Human(String name, String nickname, String lastName) {
        super(name, nickname);
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
