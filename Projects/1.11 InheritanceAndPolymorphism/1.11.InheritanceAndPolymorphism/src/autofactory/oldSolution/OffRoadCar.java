package autofactory.oldSolution;

public class OffRoadCar {

    private String brand;
    private String model;

    private boolean isAllWheels;

    public OffRoadCar(String brand, String model, boolean isAllWheels) {
        this.brand = brand;
        this.model = model;
        this.isAllWheels = isAllWheels;
    }

    public OffRoadCar() {}

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isAllWheels() {
        return isAllWheels;
    }

    public void setAllWheels(boolean allWheels) {
        isAllWheels = allWheels;
    }
}
