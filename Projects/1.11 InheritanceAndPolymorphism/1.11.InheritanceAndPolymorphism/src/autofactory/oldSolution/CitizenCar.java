package autofactory.oldSolution;

public class CitizenCar {

    private String brand;
    private String model;
    private boolean isSafe;

    public CitizenCar(String brand, String model, boolean isSafe) {
        this.brand = brand;
        this.model = model;
        this.isSafe = isSafe;
    }

    public CitizenCar() {}

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public boolean isSafe() {
        return isSafe;
    }

    public void setSafe(boolean safe) {
        isSafe = safe;
    }
}
