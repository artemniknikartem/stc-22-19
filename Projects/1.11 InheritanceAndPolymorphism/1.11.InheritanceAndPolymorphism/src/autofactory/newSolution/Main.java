package autofactory.newSolution;

public class Main {

    public static void main(String[] args) {

//        Car car = new Car("Непонятный", "Непонятная");
//
//        System.out.println(car.getBrand());
//        System.out.println(car.getModel());
//        car.drive();

        CitizenCar citizenCar = new CitizenCar("Lada", "Priora", false);
        System.out.println(citizenCar.getBrand());
        System.out.println(citizenCar.getModel());
        System.out.println(citizenCar.isSafe());
        citizenCar.drive();

        OffRoadCar offRoadCar = new OffRoadCar("Vaz", "Niva", true);

        System.out.println("************");
        System.out.println(offRoadCar.getBrand());
        System.out.println(offRoadCar.getModel());
        System.out.println(offRoadCar.isAllWheels());
        offRoadCar.drive();
//        CitizenCar vesta = new CitizenCar("LADA", "VESTA", true);
//        CitizenCar polo = new CitizenCar("WV", "Polo", true);
//
//        System.out.println(vesta.getBrand());
//        System.out.println(vesta.getModel());
//        System.out.println(vesta.isSafe());
//        System.out.println("*******************");
//        System.out.println(polo.getBrand());
//        System.out.println(polo.getModel());
//        System.out.println(polo.isSafe());
//
//        System.out.println("**********");
//
//        SportCar sportCar = new SportCar("DAEWOOE", "MATIZ", true);
//
//        System.out.println(sportCar.getModel());
//        System.out.println(sportCar.getBrand());
//        System.out.println(sportCar.isTurbo());
//
//        OffRoadCar offRoadCar = new OffRoadCar("VAZ", "2108", false);
//
//        System.out.println("*******************");
//        System.out.println(offRoadCar.getBrand());
//        System.out.println(offRoadCar.getModel());
//        System.out.println(offRoadCar.isAllWheels());
    }
}
