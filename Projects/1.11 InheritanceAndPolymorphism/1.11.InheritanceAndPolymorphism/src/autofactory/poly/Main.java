package autofactory.poly;

public class Main {

    public static void main(String[] args) {
        CitizenCar citizenCar = new CitizenCar("WV", "Polo", true);
        OffRoadCar offRoadCar = new OffRoadCar("VAZ", "Niva", true);
        SportCar sportCar = new SportCar("Ferrari", "Enzo", true);

//        citizenCar.drive();
//        offRoadCar.drive();
//        sportCar.drive();

        Car[] cars = {citizenCar, offRoadCar, sportCar};

        Math.PI
        for (int i = 0; i < cars.length; i++) {
            cars[i].drive();
        }

        citizenCar.crash();
        offRoadCar.stuck();
        sportCar.turboOn();
    }
}
