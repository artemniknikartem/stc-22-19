package singleton;

public class Main {

    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance("First Singleton");
        Singleton singleton2 = Singleton.getInstance("Second Singleton");

        System.out.println(singleton1.getMessage());
        System.out.println(singleton2.getMessage());
    }
}
