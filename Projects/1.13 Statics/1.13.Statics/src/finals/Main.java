package finals;

public class Main {

    public static void main(String[] args) {
        Human human = new Human("Oleg", "Igonin");
        SuperClass superClass = new SuperClass(10, human);
        MyClass myClass = new MyClass(10, human);

//        System.out.println(superClass.getNumber());
//        System.out.println(superClass.getHuman().getName());
//        System.out.println(superClass.getHuman().getLastName());
//        superClass.getHuman().setName("Igor");
//        System.out.println(superClass.getHuman().getName());
//        System.out.println(superClass.getHuman().getLastName());

        superClass.sayHello();


    }
}
