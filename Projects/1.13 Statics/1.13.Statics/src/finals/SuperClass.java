package finals;

public final class SuperClass {

    //если final применяется к полю - то значение данного поля можно изменить один раз
    //т.е. как только мы положили туда значение, больше это значение изменить нельзя
    public final int number;
    private final Human human;

    public SuperClass(int number, Human human) {
        this.number = number;
        this.human = human;
    }

    //Если final стоит в методе, то теперь, данный метод нельзя переопределить в классах потомках
    public final void sayHello() {
        System.out.println(human.getName() + " " + human.getLastName() + " говорит нам привет");
    }

    public int getNumber() {
        return number;
    }

    public Human getHuman() {
        return human;
    }
}
