package calculator;

public class SimpleCalculator {

    public int sumValues(int ... values) {
        int result = 0;

        for (int i = 0; i < values.length; i++) {
            result = result + values[i];
        }

        return result;
    }
}
