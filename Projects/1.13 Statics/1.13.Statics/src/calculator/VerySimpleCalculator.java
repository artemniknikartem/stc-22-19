package calculator;

public class VerySimpleCalculator {

    private int[] numbers;

    public VerySimpleCalculator(int[] numbers) {
        this.numbers = numbers;
    }

    public int sumValues() {
        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            result = result + numbers[i];
        }

        return result;
    }
}
