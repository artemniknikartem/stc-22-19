package carFactory.staticVersion;

public class Car {

    private String brand;
    private String model;
    private int vin;

    public Car(String brand, String model, int vin) {
        this.brand = brand;
        this.model = model;
        this.vin = vin;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getVin() {
        return vin;
    }
}
