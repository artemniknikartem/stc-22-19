package carFactory.nonStatic;

public class CarsFactory {

    private int vin;

    public CarsFactory() {
        this.vin = 0;
    }
    public Car createCar(String brand, String model) {
        Car car = new Car(brand, model, vin);
        vin = vin + 1;
        return car;
    }
}
