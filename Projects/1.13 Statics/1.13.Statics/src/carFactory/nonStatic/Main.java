package carFactory.nonStatic;

public class Main {

    public static void main(String[] args) {
        CarsFactory carsFactory = new CarsFactory();

        Car car = carsFactory.createCar("Mitsubishi", "Lancer");
        Car car1 = carsFactory.createCar("Mercedes-Benz", "E280");
        Car car2 = carsFactory.createCar("Audi", "A6");
        Car car3 = carsFactory.createCar("Lada", "Priora");

        System.out.println(car.getBrand());
        System.out.println(car.getModel());
        System.out.println(car.getVin());
        System.out.println("*****************");
        System.out.println(car1.getBrand());
        System.out.println(car1.getModel());
        System.out.println(car1.getVin());
        System.out.println("*****************");
        System.out.println(car2.getBrand());
        System.out.println(car2.getModel());
        System.out.println(car2.getVin());
        System.out.println("*****************");
        System.out.println(car3.getBrand());
        System.out.println(car3.getModel());
        System.out.println(car3.getVin());
        System.out.println("*****************");

        System.out.println("А теперь новый завод");

        CarsFactory carsFactory1 = new CarsFactory();

        Car car4 = carsFactory1.createCar("DAEWOO", "MATIZ");

        System.out.println(car4.getBrand());
        System.out.println(car4.getModel());
        System.out.println(car4.getVin());
    }
}
