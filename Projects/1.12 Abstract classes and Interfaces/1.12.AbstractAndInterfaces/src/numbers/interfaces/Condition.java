package numbers.interfaces;

public interface Condition {

    boolean isOk(int number);
}
