package laborExchange;

public class LaborExchangerImpl implements LaborExchanger {
    @Override
    public void takeWork(Workable workable) {
        System.out.println("Хватит сторожить кукурузы, держи работу");
        workable.work();
    }
}
