package laborExchange;

public class Main {
    public static void main(String[] args) {
        LaborExchanger laborExchanger = new LaborExchangerImpl();
        Engineer engineer = new Engineer();
        Tester tester = new Tester();
        Programmer programmer = new Programmer();
        Tiktoker tiktoker = new Tiktoker();

        laborExchanger.takeWork(engineer);
        laborExchanger.takeWork(tester);
        laborExchanger.takeWork(programmer);

    }
}
