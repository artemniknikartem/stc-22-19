package interfaces;

public class MyInterfaceRussianImpl implements MyInterface {
    @Override
    public void hello() {
        System.out.println("Привет, мы в классе МайИнтерфейсРашнИмпл");
    }

    @Override
    public void goodbay() {
        System.out.println("Пока, мы в классе МайИнтерфейсРашнИмпл");
    }

    @Override
    public void preHello() {

    }
}
