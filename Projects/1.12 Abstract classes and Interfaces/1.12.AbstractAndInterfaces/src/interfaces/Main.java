package interfaces;

public class Main {

    public static void main(String[] args) {
//        MyInterfaceImpl myInterface = new MyInterfaceImpl();
//        MyInterfaceRussianImpl myInterfaceRussian = new MyInterfaceRussianImpl();
//
//        myInterface.hello();
//        myInterface.goodbay();
//        System.out.println("****************");
//        myInterfaceRussian.hello();
//        myInterfaceRussian.goodbay();

        MyInterface myInterface = new MyInterfaceRussianImpl();
        myInterface.hello();
        myInterface.goodbay();

    }
}
