package flyable;

public class Main {
    public static void main(String[] args) {
        Flyable[] flyables = new Flyable[3];

        flyables[0] = new Airplane();
        flyables[1] = new Superman();
        flyables[2] = new Fanera();

        for (int i = 0; i < flyables.length; i++) {
            flyables[i].fly();
        }
    }
}
