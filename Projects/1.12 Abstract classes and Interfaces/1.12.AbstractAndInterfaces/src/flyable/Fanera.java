package flyable;

public class Fanera implements Flyable, Burnable {
    @Override
    public void fly() {
        System.out.println("Летим, как фанера над Парижем");
    }

    @Override
    public void burn() {
        System.out.println("*ЗВУК ГОРЕНИЯ ФАНЕРЫ*");
    }
}
