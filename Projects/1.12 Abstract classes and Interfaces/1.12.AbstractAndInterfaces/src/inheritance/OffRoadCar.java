package inheritance;

public class OffRoadCar extends Car {

    public OffRoadCar(String brand, String model) {
        super(brand, model);
    }

    @Override
    public void go() {
        System.out.println(getBrand() + " " + getModel() + " едет везде");
    }
}
