package inheritance;

public class SportCar extends Car {

    public SportCar(String brand, String model) {
        super(brand, model);
    }

    @Override
    public void go() {
        System.out.println(getBrand() + " " + getModel() + " едет оооочень быстро");
    }
}
