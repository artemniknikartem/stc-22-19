import java.io.*;

public class ReadFile {
    public static void main(String[] args) {
        try (Reader reader = new FileReader("Names.txt")) {
            //char charachter = (char) reader.read();
            //char charachter = (char) reader.read();
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
