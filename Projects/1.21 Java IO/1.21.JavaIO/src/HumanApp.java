import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class HumanApp {
    public static void main(String[] args) {
//      List<Human> humans = new ArrayList<>();
//        //TODO: по пунктам
//        humans.add(new Human("Varvara", "Igonina", "Olegovna", 2));
//        writeHumansInFile(humans);
        //реализовать CRUD модели Human -> C - create, R - read, U - update, D - delete
        List<Human> humans = getAllHumans();
        int i = 0;
    }

    public static void writeHumansInFile(List<Human> humans) {
        try (Writer writer = new FileWriter("Names.txt", true)) {
            for (Human human : humans) {
                writer.write(human.toString() + "\n");
            }
        } catch (IOException exception) {
            throw new RuntimeException();
        }
    }

    public static ArrayList<Human> getAllHumans() {
        ArrayList<Human> humans = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("Names.txt"))){
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] info = line.split("\\|");
                String lastName = info[0];
                String name = info[1];
                String patronymic = info[2];
                int age = Integer.parseInt(info[3]);
                humans.add(new Human(name, lastName, patronymic, age));
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }

        return humans;
    }
}
