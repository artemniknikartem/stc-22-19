public class O1 {
    public static void main(String[] args) {
        int[] array = new int[10000000];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }
        System.out.println(getLastElement(array));
    }

    //O(1) - константная сложность
    public static int getLastElement(int[] array) {
        int lastElement = array.length - 1;
        return array[lastElement];
    }
}
