package newVersion;

public class Main {

    public static void main(String[] args) {

        Car car = new Car("Audi", "A6", "Black");

        System.out.println(car.getBrand());
        System.out.println(car.getModel());
        System.out.println(car.getColor());

        Car car2 = new Car();

        Car builderCar = Car
                .builder()
                .brand("Mercedes-Benz")
                .build();

        System.out.println(builderCar.getBrand());
        System.out.println(builderCar.getModel());
        System.out.println(builderCar.getColor());
    }
}
